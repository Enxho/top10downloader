package academy.learnprogramming.top10downloader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class FeedAdapter extends ArrayAdapter {

    private final int layoutResource;
    private final LayoutInflater layoutInflater;
    private List<FeedEntry> applications;

    public FeedAdapter(Context context, int layoutResource, List<FeedEntry> applications) {
        super(context, layoutResource);
        this.layoutInflater = LayoutInflater.from(context);
        this.layoutResource = layoutResource;
        this.applications = applications;
    }

    @Override
    public int getCount() {
        return applications.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder ;

        if(convertView  == null){
            convertView = layoutInflater.inflate(layoutResource, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();

        }
//        TextView tvName = convertView.findViewById(R.id.tvName);
//        TextView tvArtist = convertView.findViewById(R.id.tvArtist);
//        TextView tvSummary = convertView.findViewById(R.id.tvSummary);

        FeedEntry feed = applications.get(position);
        viewHolder.tvSummary.setText(feed.getSummary());
        viewHolder.tvArtist.setText(feed.getArtist());
        viewHolder.tvName.setText(feed.getName());

        return convertView;
    }

    private class ViewHolder{
        TextView tvName;
        TextView tvArtist;
        TextView tvSummary;

        public ViewHolder(View convertView){

            this.tvName = convertView.findViewById(R.id.tvName);
            this.tvArtist = convertView.findViewById(R.id.tvArtist);
            this.tvSummary = convertView.findViewById(R.id.tvSummary);

        }
    }
}